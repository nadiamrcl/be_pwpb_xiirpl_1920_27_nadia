<html>
    <head>
    <title>Peminjaman</title>
    </head>
    <body>
        <table class="table table-fixed">
        <tr style="margin:5px">
        <th>No</th>
        <th>Id detail</th>
        <th>Pegawai</th>
        <th>Tanggal Pinjam</th>
        <th>Tanggal kembali</th>
        <th>Status</th>
        </tr>
        <?php $no=1; foreach($peminjaman as $pmnjmn):?>
                <tr style="text-align:center">
                    <th scope="row"><?= $no++ ?></th>
                    <td><?= $pmnjmn->id_detail_pinjam;?></td>
                    <td><?= $pmnjmn->id_pegawai;?></td>
                    <td><?= $pmnjmn->tgl_pinjam;?></td>
                    <td><?= $pmnjmn->tgl_kembali;?></td>
                    <td><?= $pmnjmn->status_peminjaman;?></td>
                 
            </tr>
        <?php endforeach; ?>
        </table>

        <script type="text/javascript">
            window.print();
        </script>
    </body>
</html>
