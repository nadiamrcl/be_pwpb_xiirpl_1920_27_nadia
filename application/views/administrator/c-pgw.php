

<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="border-right" id="sidebar-wrapper">
      <div class="sidebar-heading" style="color:#fff;background-color: #00A18E">INVENCHOOL</div>

<center>
      <div class="sidebar-heading" style="color:#fff;background-color: rgb(255, 255, 255);margin:10px;margin-bottom: 0;width: 160px">
      <img src="<?php echo base_url('assets/img/petugas/'); ?><?= $this->session->userdata('gambar');?>" style="background-size:cover;width: 100%;border-radius: 50%">
      </div>

      <div class="sidebar-heading" style="color:#000;background-color: rgb(255, 255, 255);width: 200px;text-align: center;font-family: calibri;margin-left: 10px;margin-right: 10px">
        <h5><?= $this->session->userdata('username');?></h5>
        <p style="color:rgb(180, 177, 177);font-size: 12pt">Administrator</p>
        
        </div>
</center>

<div class="list-group list-group-flush" >
          <a href="<?= base_url ('Admin/index');?>" class="list-group-item list-group-item-actions a"><i class="fas fa-chart-line"></i>
            Dashboard</a>
  
            <a href="#" class="dropdown-toggle list-group-item list-group-item-action a" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i><i class="fas fa-home"></i> Home <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="<?= base_url ('Admin/brg');?>" class="list-group-item list-group-item-actions a">List Barang</a></li>
                  <li><a href="<?= base_url ('Admin/pgw');?>" class="list-group-item list-group-item-actions a">List Pegawai</a></li>
                  <li><a href="<?= base_url ('Admin/peminjaman');?>" class="list-group-item list-group-item-actions a">List Peminjaman</a></li>
                  <li><a href="<?= base_url ('Admin/pengembalian');?>" class="list-group-item list-group-item-actions a">List Pengembalian</a></li>
                </ul>
  
  
  
          <a href="<?= base_url ('Admin/op');?>" class="list-group-item list-group-item-action a"><i class="fas fa-user-shield"></i> Operator</a>
          <a href="<?= base_url ('Admin/ruang');?>" class="list-group-item list-group-item-action a"><i class="fas fa-building"></i> Ruang</a>
          <a href="<?= base_url ('Admin/j_brg');?>" class="list-group-item list-group-item-action a"><i class="fas fa-clipboard-list"></i> Jenis barang</a>
          <a href="<?= base_url ('Admin/setting');?>" class="list-group-item list-group-item-action a"><i class="fas fa-cog"></i> Account</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->






    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg border-bottom" style="background-color:#00A18E">
        <button class="btn" id="menu-toggle"><i class="fas fa-bars" style="color: #fff"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">



          
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
              <li class="nav-item nav-search d-none d-lg-block">                
                    <input type="text" class="form-control" id="navbar-search-input" placeholder="Search now" aria-label="search" aria-describedby="search">
              </li>

            <li class="nav-item" style="margin-left:5px">
              <a class="nav-link" href="<?= base_url ('Landing/index');?>" style="color:black">log out <span class="sr-only"></span></a>
            </li>
          </ul>
        </div>
      </nav>


<!--start-->
      <div class="container-fluid"style="background-color:none;margin-top: 50px;margin-bottom:50px">
        <div class="row col-lg-12 justify-content-center">
<!-- Material form contact -->
<div class="card" style="border:1px solid rgb(214, 214, 214)">

    <h5 class="card-header  white-text text-left py-4" style="background-color:#00A18E;font-family:tw cen mt;color:#fff">
        <strong>Tambah Pegawai</strong>
    </h5>

    <!--Card content-->
    <div class="card-body pt-0" style="width:700px;">

        <!-- Form -->
        <form class="text-center" style="color: #ada5a5;margin:50px" action="<?php echo base_url('Admin/tambah_pgw'); ?>" method="post" enctype="multipart/form-data">

            <!-- Nama-->
            <div class="md-form mt-3">
                <input type="text" name="nama_pgw" id="materialContactFormName" class="form-control" placeholder="Nama" required>

            </div>            
            
            <!-- username-->
            <div class="md-form mt-3">
                <input type="text" name="username" id="materialContactFormName" class="form-control" placeholder="User Name" required>

            </div>            
             
            <!-- Email-->
            <div class="md-form mt-3">
                <input type="email" name="email" id="materialContactFormName" class="form-control" placeholder="Email" required>

            </div>                       
             
            <!-- password-->
            <div class="md-form mt-3">
                <input type="text"  name="password" id="materialContactFormName" class="form-control" placeholder="password" required>

            </div> 

            <!-- NIP -->
            <div class="md-form mt-3">
                <input type="text" name="nip" id="materialContactFormName" class="form-control" placeholder="NIP" required>

            </div>

            <!-- alamat -->
             <div class="md-form mt-3">
                <input type="text" name="alamat" id="materialContactFormName" class="form-control" placeholder="Alamat">

            </div>           






            <!-- Image-->
            <div class="md-form mt-3">
                <input type="file"  name="gambar" placeholder="Image" style="margin:5px">
            </div><br>


            <!-- Send button -->
            <input type="submit" value="tambah" class="btn btn-outline-info btn-rounded waves-effect">
            <a href="<?php echo base_url('Admin/pgw'); ?>" class="btn btn-outline-danger btn-rounded waves-effect">Cancel</a>


        </form>
        <!-- Form -->
</div>
    </div>

</div>
<!-- Material form contact -->
  
            


<!--end-->
            

    </div><!-- /.row -->




</div>
</div>


</div>


<!-- /#page-content-wrapper -->
<!-- /#page-content-wrapper -->

</div>

 <!-- div4 -->
 <div class="container-fluid" style="background-image:url(<?php echo base_url(); ?>/assets/img/fot.png);background-size: cover ;">
  <div class="row justify-content-md-center">
    <div class="col-3" style="background-color: none;">
     <i class="fab fa-whatsapp icon"></i> <b class="white fontfot">089-9796-1365</b><br>
     <i class="fab fa-whatsapp icon"></i> <b class="white fontfot">invenchool21@gmail.com</b>
    </div>

    <div class="col-3" style="background-color: none;">
     <p class="icon" style="margin-top:20px;color: #fff"> About Invenschool</p>
     <p class="white" style="font-size:13">ipsumloream ipsumloremloream, ipsumloream ipsumloream ipsumloream remloream, ipsumloream ips</p>
    
     <i class="fab fa-instagram icon"></i>
     <i class="fab fa-twitter icon" style="margin-left:5px;margin-right:5px"></i>
     <i class="fab fa-facebook icon"></i>
    </div>
  </div><br><br>




 </div>
<!--div5-->
<div class="container-fluid" style="background-color:#00A18E;height: 50px;">
  <center><p class="white fontfot" style="margin-top:0;padding-top:10px">@yoowray</p></center>
</div>

<!-- /#wrapper -->

<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url(); ?>/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Menu Toggle Script -->
<script>
$("#menu-toggle").click(function(e) {
e.preventDefault();
$("#wrapper").toggleClass("toggled");
});

$('#dtBasicExample').mdbEditor({
mdbEditor: true
});
$('.dataTables_length').addClass('bs-select');
</script>


</body>

</html>

