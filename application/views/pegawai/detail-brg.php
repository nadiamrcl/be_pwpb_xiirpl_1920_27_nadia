<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Barang Pegawai</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url(); ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">


  <!-- Custom styles for this template -->
  <link href="<?php echo base_url(); ?>/assets/css/sidebar.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/assets/css/landingpage.min.css" rel="stylesheet">

  

</head>

<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="border-right" id="sidebar-wrapper">
      <div class="sidebar-heading" style="color:#fff;background-color: #00A18E">INVENCHOOL</div>

<center>
      <div class="sidebar-heading" style="color:#fff;background-color: rgb(255, 255, 255);margin:10px;margin-bottom: 0;width: 160px">
      <img src="<?php echo base_url('assets/img/pegawai/'); ?><?= $this->session->userdata('gambar');?>" style="background-size:cover;width: 100%;border-radius: 150px">
      </div>

      <div class="sidebar-heading" style="color:#000;background-color: rgb(255, 255, 255);width: 200px;text-align: center;font-family: calibri;margin-left: 10px;margin-right: 10px">
        <h5><?= $this->session->userdata('username');?></h5>
        <p style="color:rgb(180, 177, 177);font-size: 12pt">Pegawai / Staff</p>
        
        </div>
</center>

      <div class="list-group list-group-flush" >
          <a href="<?= base_url ('Pegawai/index');?>" class="list-group-item list-group-item-actions a"><i class="fas fa-stamp"></i> Barang</a>
          <a href="<?= base_url ('Pegawai/settings');?>" class="list-group-item list-group-item-action a"><i class="fas fa-cog"></i> Settings</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg border-bottom" style="background-color:#00A18E">
        <button class="btn" id="menu-toggle"><i class="fas fa-bars" style="color: #fff"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">



          
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
              <li class="nav-item nav-search d-none d-lg-block">                
                    <input type="text" class="form-control" id="navbar-search-input" placeholder="Search now" aria-label="search" aria-describedby="search">
              </li>

            <li class="nav-item" style="margin-left:5px">
              <a class="nav-link" href="<?= base_url ('Landing/index');?>" style="color:black">log out <span class="sr-only"></span></a>
            </li>
    </ul>
  </div>
</nav>

<div class="container-fluid"style="background-color:none;margin-top: 50px;">


<div class="row u" style="margin-bottom:50px">
<h3 style="text-align:left;padding-left: 20px;color: #fff">Detail Barang</h3>
  <div class="col col-md-12" style="background-color:rgb(255, 255, 255);margin-top: 10px">

      <div class="row" style="margin-top:50px;margin-bottom: 50px">        
          <div class="col col-lg-6" style="background-color:rgb(255, 255, 255)">
              <div class="col-lg-9" style="background-color:rgb(255, 255, 255);margin: auto">
          
          <?php foreach($vinventaris as $v):?>     
   
          <img src="<?php echo base_url('/assets/img/barang/'). $v['gambar'];?>" style="width:100%;border-radius: 10px;border:1px solid #dee2e6">
              </div>
          </div>


          <div class="col col-lg-6" style="background-color:rgb(255, 255, 255); font-family: tw cen mt">
          
         
                 <h4><?= $v['nama'];?></h4><br>
                 <p>ID : <?= $v['id_inventaris'];?></p> 
                 <p>Jumlah : <?= $v['jumlah'];?></p>
                 <p>Jenis : <?= $v['nama_jenis'];?></p> 
                 <p>Ruang : <?= $v['nama_ruang'];?></p>                     
                 <p>Kode Barang : <?= $v['kode_inventaris'];?></p>  
                 <p>Kondisi : <?= $v['kondisi'];?></p>
                 <p>Keterangan : <?= $v['keterangan'];?></p>
                <p>Nama petugas : <?= $v['nama_ptg'];?></p> 
          
          </div>
      </div>
      
  <center>
      <a href="<?= base_url ('Pegawai/peminjaman/'. $v['id_inventaris']); ?>" class="btn btn-info btn-rounded " style="margin-top:50px;margin-left:20px;margin-bottom: 50px">Pinjam</a>        
      <a href="<?= base_url ('Pegawai/brg');?>" class="btn btn-danger btn-rounded " style="margin-top:50px;margin-left:20px;margin-bottom: 50px">Kembali</a>

  </center>
 <?php endforeach ?> 
  </div>

      


<!--end-->
      

</div><!-- /.row -->





</div>
</div>


</div>


<!-- /#page-content-wrapper -->

</div>


 <!-- div4 -->
 <div class="container-fluid" style="background-image:url(<?php echo base_url(); ?>/assets/img/fot.png);background-size: cover ;">
    <div class="row justify-content-md-center">
      <div class="col-3" style="background-color: none;">
       <i class="fab fa-whatsapp icon"></i> <b class="white fontfot">089-9796-1365</b><br>
       <i class="fab fa-whatsapp icon"></i> <b class="white fontfot">invenchool21@gmail.com</b>
      </div>
  
      <div class="col-3" style="background-color: none;">
       <p class="icon" style="margin-top:20px;color: #fff"> About Invenschool</p>
       <p class="white" style="font-size:13">ipsumloream ipsumloremloream, ipsumloream ipsumloream ipsumloream remloream, ipsumloream ips</p>
      
       <i class="fab fa-instagram icon"></i>
       <i class="fab fa-twitter icon" style="margin-left:5px;margin-right:5px"></i>
       <i class="fab fa-facebook icon"></i>
      </div>
    </div><br><br>
  
  
  
  
   </div>
  <!--div5-->
  <div class="container-fluid" style="background-color:#00A18E;height: 50px;">
    <center><p class="white fontfot" style="margin-top:0;padding-top:10px">@yoowray</p></center>
  </div>
  
<!-- /#wrapper -->

<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url(); ?>/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Menu Toggle Script -->
<script>
$("#menu-toggle").click(function(e) {
e.preventDefault();
$("#wrapper").toggleClass("toggled");
});

$('#dtBasicExample').mdbEditor({
mdbEditor: true
});
$('.dataTables_length').addClass('bs-select');
</script>


</body>

</html>

