<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Barang Pegawai</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url(); ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">


  <!-- Custom styles for this template -->
  <link href="<?php echo base_url(); ?>/assets/css/sidebar.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/assets/css/landingpage.min.css" rel="stylesheet">

  

</head>

<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="border-right" id="sidebar-wrapper">
      <div class="sidebar-heading" style="color:#fff;background-color: #00A18E">INVENCHOOL</div>

<center>
      <div class="sidebar-heading" style="color:#fff;background-color: rgb(255, 255, 255);margin:10px;margin-bottom: 0;width: 160px">
      <img src="<?php echo base_url('assets/img/pegawai/'); ?><?= $this->session->userdata('gambar');?>" style="background-size:cover;width: 100%;border-radius: 150px">
      </div>

      <div class="sidebar-heading" style="color:#000;background-color: rgb(255, 255, 255);width: 200px;text-align: center;font-family: calibri;margin-left: 10px;margin-right: 10px">
        <h5><?= $this->session->userdata('username');?></h5>
        <p style="color:rgb(180, 177, 177);font-size: 12pt">Pegawai / Staff</p>
        
        </div>
</center>

      <div class="list-group list-group-flush" >
          <a href="<?= base_url ('Pegawai/index');?>" class="list-group-item list-group-item-actions a"><i class="fas fa-stamp"></i> Barang</a>
          <a href="<?= base_url ('Pegawai/settings');?>" class="list-group-item list-group-item-action a"><i class="fas fa-cog"></i> Settings</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg border-bottom" style="background-color:#00A18E">
        <button class="btn" id="menu-toggle"><i class="fas fa-bars" style="color: #fff"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">



          
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
              <li class="nav-item nav-search d-none d-lg-block">                
                    <input type="text" class="form-control" id="navbar-search-input" placeholder="Search now" aria-label="search" aria-describedby="search">
              </li>

            <li class="nav-item" style="margin-left:5px">
              <a class="nav-link" href="<?= base_url ('Landing/index');?>" style="color:black">log out <span class="sr-only"></span></a>
            </li>
          </ul>
        </div>
      </nav>



<!--start-->
      <div class="container-fluid"style="background-color:none;margin-top: 50px;margin-bottom:50px;margin: 60px">
        <div class="row col-lg-12 ">
<!-- Material form contact -->
<div class="card" style="border:1px solid rgb(214, 214, 214)">

    <h5 class="card-header  white-text text-left py-4" style="background-color:#00A18E;font-family:tw cen mt;color:#fff">
        <strong>My Profile</strong>
    </h5>

    <!--Card content-->
    <div class="card-body pt-0 " style="width:500px;">

        <!-- Form -->
        <center>
        <div class="col" style="width:200px;margin-top:50px">
                <img src="<?= base_url('assets/img/pegawai/'); ?><?= $this->session->userdata('gambar')?>" style="background-size:cover;width: 100%;border-radius: 50%">
            </div>
      </center>
        </div>

        <form class="text-center" style="color: #ada5a5;margin-left:50px;margin-left:50px;margin-bottom: 50px;margin-top: 3px;margin-right: 50px" action="#!">
            
            <!-- Id Profile -->

            <!-- ID NIP -->
            <div class="md-form mt-2">
            <p style="text-align:left;"><?php echo " NIP";?></p>              
                <input type="text" id="materialContactFormName" class="form-control" value="<?= $this->session->userdata('nip')?>" disabled>

            </div>

            <!-- Nama-->
            <div class="md-form mt-2">
            <p style="text-align:left;"><?php echo "Nama";?></p>
            <input type="text" id="materialContactFormName" class="form-control" value="<?= $this->session->userdata('nama_pgw')?>" disabled>

            </div>

            <!-- User-Name-->
            <div class="md-form mt-2">
            <p style="text-align:left;"><?php echo " User Name";?></p>              
            <input type="text" id="materialContactFormName" class="form-control" value="<?= $this->session->userdata('username')?>" disabled>

            </div>

            <!-- Email-->
            <div class="md-form mt-2">
            <p style="text-align:left;"><?php echo " Email";?></p> 
            <input type="email" id="materialContactFormName" class="form-control" value="<?= $this->session->userdata('email')?>" disabled>

            </div>


            <!-- Send button -->
<!--             <a href="settings.html" class="btn btn-outline-info btn-rounded waves-effect">Save</a>
            <a href="settings.html" class="btn btn-outline-danger btn-rounded waves-effect">Cancle</a>
         --></form>
        <!-- Form -->
</div>

        <!-- card-body 2 -->
<!--             <div class="col" style="background-color:none;margin-top: 50px;width: 350px;">
              <div class="col-md-6 " style="background-color:#fff;margin: 20px;margin-left:180px;border-radius: 10px;border:1px solid rgb(214, 214, 214)">
                <div class="col" style="background-color:rgb(255, 255, 255);text-align:">
                <center>
                  <div class="col" style="width:150px;">
                          <img src="../../assets/img/face4.jpg" style="background-size:cover;width: 100%;border-radius: 50%;margin-top: 10px">
                      </div>
                </center>

                
                  <p class="text-p margin-top">Your Name</p>
                  <p class="text-p">Your UserName</p>
                  <p class="text-p">Oprator 01</p>
                  <p class="text-p">Email@gmail.com</p>

                </div>
              </div>
            </div> -->
        <!-- end card-body2 -->
          
            

</div>
</div>
<!-- Material form contact -->
  


<!--end-->
            

    </div><!-- /.row -->




</div>
</div>


</div>


<!-- /#page-content-wrapper -->
<!-- /#page-content-wrapper -->

</div>

 <!-- div4 -->
 <div class="container-fluid" style="background-image:url(<?php echo base_url(); ?>/assets/img/fot.png);background-size: cover ;">
  <div class="row justify-content-md-center">
    <div class="col-3" style="background-color: none;">
     <i class="fab fa-whatsapp icon"></i> <b class="white fontfot">089-9796-1365</b><br>
     <i class="fab fa-whatsapp icon"></i> <b class="white fontfot">invenchool21@gmail.com</b>
    </div>

    <div class="col-3" style="background-color: none;">
     <p class="icon" style="margin-top:20px;color: #fff"> About Invenschool</p>
     <p class="white" style="font-size:13">ipsumloream ipsumloremloream, ipsumloream ipsumloream ipsumloream remloream, ipsumloream ips</p>
    
     <i class="fab fa-instagram icon"></i>
     <i class="fab fa-twitter icon" style="margin-left:5px;margin-right:5px"></i>
     <i class="fab fa-facebook icon"></i>
    </div>
  </div>
<br><br>



 </div>
<!--div5-->
<div class="container-fluid" style="background-color:#00A18E;height: 50px;">
  <center><p class="white fontfot" style="margin-top:0;padding-top:10px">@yoowray</p></center>
</div>

<!-- /#wrapper -->

<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url(); ?>/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Menu Toggle Script -->
<script>
$("#menu-toggle").click(function(e) {
e.preventDefault();
$("#wrapper").toggleClass("toggled");
});

$('#dtBasicExample').mdbEditor({
mdbEditor: true
});
$('.dataTables_length').addClass('bs-select');
</script>


</body>

</html>

