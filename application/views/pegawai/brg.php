<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Barang Pegawai</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url(); ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">


  <!-- Custom styles for this template -->
  <link href="<?php echo base_url(); ?>/assets/css/sidebar.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/assets/css/landingpage.min.css" rel="stylesheet">

  

</head>

<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="border-right" id="sidebar-wrapper">
      <div class="sidebar-heading" style="color:#fff;background-color: #00A18E">INVENCHOOL</div>

<center>
      <div class="sidebar-heading" style="color:#fff;background-color: rgb(255, 255, 255);margin:10px;margin-bottom: 0;width: 160px">
      <img src="<?php echo base_url('assets/img/pegawai/'); ?><?= $this->session->userdata('gambar');?>" style="background-size:cover;width: 100%;border-radius: 150px">
      </div>

      <div class="sidebar-heading" style="color:#000;background-color: rgb(255, 255, 255);width: 200px;text-align: center;font-family: calibri;margin-left: 10px;margin-right: 10px">
        <h5><?= $this->session->userdata('username');?></h5>
        <p style="color:rgb(180, 177, 177);font-size: 12pt">Pegawai / Staff</p>
        
        </div>
</center>

      <div class="list-group list-group-flush" >
          <a href="<?= base_url ('Pegawai/index');?>" class="list-group-item list-group-item-actions a"><i class="fas fa-stamp"></i> Barang</a>
          <a href="<?= base_url ('Pegawai/settings');?>" class="list-group-item list-group-item-action a"><i class="fas fa-cog"></i> Settings</a>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->






    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg border-bottom" style="background-color:#00A18E">
        <button class="btn" id="menu-toggle"><i class="fas fa-bars" style="color: #fff"></i></button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">



          
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
              <li class="nav-item nav-search d-none d-lg-block">                
                    <input type="text" class="form-control" id="navbar-search-input" placeholder="Search now" aria-label="search" aria-describedby="search">
              </li>

            <li class="nav-item" style="margin-left:5px">
              <a class="nav-link" href="<?= base_url ('Landing/index');?>" style="color:black">log out <span class="sr-only"></span></a>
            </li>
            
            <!-- <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li> -->
          </ul>
        </div>
      </nav>

      <div class="container-fluid">
        <div class="row" style="margin-top:20px" >
          <!-- <div class="col-lg-12">
            <ol class="breadcrumb">
              <li><a href="#"><i class="icon-dashboard"></i> Barang</a></li>
              <li class="active"><i class="icon-file-alt"></i>/</li>
            </ol>
          </div> -->
        </div><!-- /.row -->
        <br>

  <div class="row left" style="background-color:none">
  <!--   <h4>Barang</h4> -->
      
  </div>
  <div class="row justify-content-md" style="margin-right:20px;margin-left:5px;margin-top:50px;margin-bottom:50px">
    <?php foreach($vinven_nol as $i):?>
        <div class="card" style="margin:10px;">
          <div class="card-body" style="width: 20rem;">          
          <img src="<?php echo base_url('/assets/img/barang/'). $i['gambar'];?>" class="card-img-top" alt="..." style="width:100%">
          </div> 
          <center>         
          <a href="<?= base_url ('Pegawai/dbrg/'. $i['id_inventaris']); ?>" class="btn btn-outline-info btn-rounded waves-effect" style="height:50px; width:120px;margin-bottom:10px; font-size:15pt;padding-top: 12px; font-family: ink free"><i class="far fa-hand-pointer" style="font-size: 18pt"></i> Lihat</a>

          <a href="<?= base_url ('Pegawai/peminjaman/'. $i['id_inventaris']); ?>" class="btn btn-outline-info btn-rounded waves-effect" style="height:50px; width:120px;margin-bottom:10px; font-size:15pt;padding-top: 12px; font-family: ink free"><i class="fas fa-dolly" style="font-size: 18pt"></i> Pinjam</a>          
          </center>
        </div>
    <?php endforeach?>
  </div>
  




<!--end-->
</div>




</div>
</div>


</div>







<!-- /#page-content-wrapper -->

</div>

 <!-- div4 -->
 <div class="container-fluid" style="background-image:url(<?php echo base_url(); ?>/assets/img/fot.png);background-size: cover ;">
  <div class="row justify-content-md-center">
    <div class="col-3" style="background-color: none;">
     <i class="fab fa-whatsapp icon"></i> <b class="white fontfot">089-9796-1365</b><br>
     <i class="fab fa-whatsapp icon"></i> <b class="white fontfot">invenchool21@gmail.com</b>
    </div>

    <div class="col-3" style="background-color: none;">
     <p class="icon" style="margin-top:20px;color: #fff"> About Invenschool</p>
     <p class="white" style="font-size:13">ipsumloream ipsumloremloream, ipsumloream ipsumloream ipsumloream remloream, ipsumloream ips</p>
    
     <i class="fab fa-instagram icon"></i>
     <i class="fab fa-twitter icon" style="margin-left:5px;margin-right:5px"></i>
     <i class="fab fa-facebook icon"></i>
    </div>
  </div>
<br><br>



 </div>
<!--div5-->
<div class="container-fluid" style="background-color:#00A18E;height: 50px;">
  <center><p class="white fontfot" style="margin-top:0;padding-top:10px">@yoowray</p></center>
</div>

<!-- /#wrapper -->

<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url(); ?>/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Menu Toggle Script -->
<script>
$("#menu-toggle").click(function(e) {
e.preventDefault();
$("#wrapper").toggleClass("toggled");
});

$('#dtBasicExample').mdbEditor({
mdbEditor: true
});
$('.dataTables_length').addClass('bs-select');
</script>


</body>

</html>

