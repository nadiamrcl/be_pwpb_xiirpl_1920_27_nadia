<?php 
 
class Crud_model extends CI_Model{
	function tampil_data(){
		return $this->db->get('inventaris');
		return $this->db->get('pegawai');
		
		
	}
	function print(){
		return $this->db->get('peminjaman');
	}
 
	function input_data($data,$table){
		// var_dump($data);die();
		$this->db->insert($table,$data);
	}

	function hapus_data($where, $table){
        $this->db->where($where);
        $this->db->delete($table);
	}
	
	function edit_data($where, $table){
		return $this->db->get_where($table, $where);
	}

	function update_data($where, $data, $table){
		$this->db->where($where);
        $this->db->update($table, $data);
	}

	function detail_data($where, $table){
		return $this->db->get_where($table, $where)->result_array();
	}
}