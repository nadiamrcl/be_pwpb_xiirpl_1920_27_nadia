<?php

Class Op_model extends CI_Model{
    public function getAlljmlpgw(){
        return $this->db->get('jml_pgw')->result_array();
    }

    public function getAlljmlbrg(){
        return $this->db->get('jml_brg')->result_array();
    }

    public function getAlljmlpinjam(){
        return $this->db->get('jum_pinjam')->result_array();
    }

    public function getAlljmlkembali(){
        return $this->db->get('jum_kembali')->result_array();
    }
        
    public function getAllbrg(){
        return $this->db->get('inventaris')->result_array();
    }
    
    public function getAllviewbrg(){
        return $this->db->get('vinven')->result_array();
    }
    

    public function getAllpgw(){
        return $this->db->get('pegawai')->result_array();
    }

    public function getAllpeminjaman(){
        return $this->db->get('peminjaman')->result_array();
    }

    public function getAlldpinjam(){
        return $this->db->get('detail_pinjam')->result_array();
    }

    //view peminjaman
    public function getAllvpeminjaman(){
        return $this->db->get('vpeminjaman')->result_array();
    }

    public function getAllpengembalian(){
        return $this->db->get('pengembalian')->result_array();
    }
    
    //view pengembalian
    public function getAllvpengembalian(){
        return $this->db->get('vpengembalian')->result_array();
    }

    function input($table,$data){
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }

    //batas


    public function getAlljenisbrg(){
        return $this->db->get('jenis')->result_array();
    }

    public function getAllruang(){
        return $this->db->get('ruang')->result_array();
    }

    public function getAllptg(){
        return $this->db->get('petugas')->result_array();
    }

}


 ?>