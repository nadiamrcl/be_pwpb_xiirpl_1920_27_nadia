<?php

Class Admin_model extends CI_Model{     


    public function getAlljmlpgw(){
        return $this->db->get('jml_pgw')->result_array();
    }

    public function getAlljmlbrg(){
        return $this->db->get('jml_brg')->result_array();
    }

    public function getAlljmlpinjam(){
        return $this->db->get('jum_pinjam')->result_array();
    }

    public function getAlljmlkembali(){
        return $this->db->get('jum_kembali')->result_array();
    }

    public function getAllbrg(){
        return $this->db->get('inventaris')->result_array();
    }


    public function getAllviewbrg(){
        return $this->db->get('vinven')->result_array();
    }

    public function getbrg_keyword($keyword){
        $this->db->select('*');
        $this->db->from('vinven');
        $this->db->like('nama',$keyword);
        // $this->db->or_like('harga',$keyword);
        return $this->db->get()->result();
    }

    public function getAllpgw(){
        return $this->db->get('pegawai')->result_array();
    }

    public function getAllpeminjaman(){
        return $this->db->get('peminjaman')->result_array();
    }

    public function getAllpengembalian(){
        return $this->db->get('pengembalian')->result_array();
    }

        //view pengembalian
    public function getAllvpengembalian(){
        return $this->db->get('vpengembalian')->result_array();
    }

    //view peminjaman
    public function getAllvpeminjaman(){
        return $this->db->get('vpeminjaman')->result_array();
    }

    public function getAlljenisbrg(){
        return $this->db->get('jenis')->result_array();
    }

    public function getAllruang(){
        return $this->db->get('ruang')->result_array();
    }

    public function getAllptg(){
        return $this->db->get('petugas')->result_array();
    }

    public function cbrg(){
        $data=[
            "id_inventaris" =>$this->input->post('id_brg', true),
            "id_ptg" =>$this->input->post('id_ptg', true),
            "id_jenis" =>$this->input->post('id_jenis', true),
            "id_ruang" =>$this->input->post('id_ruang', true),
            "nama" =>$this->input->post('nama', true),
            "kondisi" =>$this->input->post('kondisi', true),
            "ket" =>$this->input->post('ket', true),
            "jumlah" =>$this->input->post('jumlah', true),
            "tgl_regis" =>$this->input->post('tgl', true),
            "kode_inventaris" =>$this->input->post('kode', true),
            "gambar" =>$this->input->post('pic', true),
        ];

        $this->db->insert('inventaris', $data);
    }

    public function cpgw(){
        //semua field di table, right=db left=form
        $data=[
            "id_pegawai" =>$this->input->post('id_pegawai', true),
            "nama_pgw" =>$this->input->post('nama', true),
            "username" =>$this->input->post('username', true),
            "email" =>$this->input->post('email', true),
            "password" =>$this->input->post('password', true),
            "nip" =>$this->input->post('nip', true),
            "gambar" =>$this->input->post('gambar', true),
            "alamat" =>$this->input->post('alamat', true)
        ];

        $this->db->insert('pegawai', $data);
    }


    public function cj_brg(){
        $data=[
           "id_jenis" =>$this->input->post('id_jenis', true),
           "nama_jenis" =>$this->input->post('nama', true),
           "kode_jenis" =>$this->input->post('kode', true),
           "keterangan" =>$this->input->post('ket', true)
        ];

        

        $this->db->insert('jenis', $data);
    }
}


 ?>