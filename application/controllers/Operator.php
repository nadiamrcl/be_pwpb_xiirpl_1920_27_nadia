
<?php

class Operator extends CI_Controller {
    function __construct(){

    parent::__construct();

    $this->load->helper('url');
    $this->load->helper('form');
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->model('Op_model');
    $this->load->model('Crud_model');
    
    }

    function index(){
        $data['inventaris'] = $this->Op_model->getAllbrg();
        $data['jml_pgw'] = $this->Op_model->getAlljmlpgw(); 
        $data['jml_brg'] = $this->Op_model->getAlljmlbrg();  
        $data['jum_pinjam'] = $this->Op_model->getAlljmlpinjam(); 
        $data['jum_kembali'] = $this->Op_model->getAlljmlkembali(); 


        $this->load->view('operator/dashboard', $data);
        // $this->load->view('templates/footer');
    }

    // brg=========================================

        function brg(){
            $data['judul'] = 'Halaman Barang';
            $data['inventaris'] = $this->Op_model->getAllbrg();
            $data['vinven'] = $this->Op_model->getAllviewbrg();

            $this->load->view('templates/header2', $data);
            $this->load->view('operator/brg',$data);
        }

        function dbrg($id){
            $data['judul'] = 'O.Detail Barang';
            $where = array('id_inventaris'=>$id);
            $data['vinventaris'] = $this->Crud_model->detail_data($where, 'vinventaris');
            
            $this->load->view('templates/header2', $data);
            $this->load->view('operator/detail-brg', $data);
        }

    // pgw==========================================
        function pgw(){
        $data['judul'] = 'O.Pegawai';
        $data['pgw'] = $this->Op_model->getAllpgw();

        $this->load->view('templates/header2', $data);
        $this->load->view('operator/pgw', $data);
        }

        function cpgw(){
            $data['judul'] = 'O.C-Pegawai';
           
            $this->load->view('templates/header2', $data);
            $this->load->view('operator/c-pgw', $data);   
        }   

        function tambah_pgw(){
            $config['upload_path']   = 'assets/img/pegawai/';
            $config['allowed_types']   = 'gif|jpg|png|jpeg';
            $config['max_size']         = 10000;
          
          
              $this->load->library('upload', $config);
                $post   = $_FILES['gambar'];
                $file_data =array(
                    "name" =>$post["name"],
                    "type" =>$post["type"],
                    "tmp_name" =>$post["tmp_name"],
                    "error" =>$post["error"],
                    "size" =>$post["size"]
                );
            // var_dump( $post);die();

            $dataUpload  =$file_data;
            $nama_file  ="brg"."-".time()."-".str_replace("", "_", $post['name']);

           // var_dump($nama_file);die();
            move_uploaded_file($post['tmp_name'], 'assets/img/pegawai/'.$nama_file);

            $id = $this->input->post('id_pegawai');
            $nama = $this->input->post('nama_pgw');
            $username = $this->input->post('username');
            $email = $this->input->post('email');
            $pass = $this->input->post('password');
            $nip = $this->input->post('nip');
            $gambar = $this->input->post('gambar');
            $alamat = $this->input->post('alamat');
        
            $data = array(
                'nama_pgw' => $nama,
                'username' => $username,
                'email' => $email,
                'password' => password_hash($pass, PASSWORD_DEFAULT),
                'nip' => $nip,
                'gambar' => $nama_file,
                'alamat' => $alamat
                );
                
            $this->db->insert('pegawai', $data);
            redirect('Operator/pgw');
        }

        function hapus_pgw($id){
            $where= array('id_pegawai'=> $id);
            $this->Crud_model->hapus_data($where, 'pegawai');
            redirect('Operator/pgw');
        }

        function dpgw($id){
            
            $data['judul'] = 'O.Detail pegawai';
            $where = array('id_pegawai'=>$id);
            $data['pegawai'] = $this->Crud_model->detail_data($where, 'pegawai');
            
            $this->load->view('templates/header2', $data);
            $this->load->view('operator/detail-pgw', $data);
        }

        function upgw($id){
            $data['judul'] = 'O.U-Pegawai';
            $where = array('id_pegawai'=>$id);
            $data['pegawai'] = $this->Crud_model->edit_data($where, 'pegawai')->result();
    
            $this->load->view('templates/header2', $data);
            $this->load->view('operator/u-pgw', $data);
        }
    
        function update_pgw(){
            $id = $this->input->post('id_pegawai');
            $nama = $this->input->post('nama_pgw');
            $username = $this->input->post('username');
            $email = $this->input->post('email');
            $pass = $this->input->post('password');
            $nip = $this->input->post('nip');
            $gambar = $this->input->post('gambar');
            $alamat = $this->input->post('alamat');
        
            $data = array(
                'nama_pgw' => $nama,
                'username' => $username,
                'email' => $email,
                'password' => $pass,
                'nip' => $nip,
                'gambar' => $gambar,
                'alamat' => $alamat
                );
    
                $where = array('id_pegawai'=>$id);
                $this->Crud_model->update_data($where,$data, 'pegawai');
                redirect('Operator/pgw');
        }

    // peminjaman===================================
        function peminjaman(){
        $data['judul'] = 'Hal Peminjaman A';
        $data['peminjaman'] = $this->Op_model->getAllpeminjaman();
        $data['vpeminjaman'] = $this->Op_model->getAllvpeminjaman();
        $data['detail_pinjam'] = $this->Op_model->getAllvpeminjaman();



        $this->load->view('templates/header2', $data);
        $this->load->view('operator/peminjaman', $data);
        }

        function c_peminjaman(){
            $data['judul'] = 'O.C-peminjaman';

            $data['pegawai'] = $this->db->get('pegawai')->result_array();
            $data['inventaris'] = $this->db->get('inventaris')->result_array();
            $data['peminjaman'] = $this->db->get('peminjaman')->result_array();
            $data['detail_pinjam'] = $this->db->get('detail_pinjam')->result_array();


            $this->load->view('templates/header2', $data);
            $this->load->view('operator/c-peminjaman', $data);   
        }   

        function tambah_peminjaman(){
            $id_pegawai = $this->input->post('id_pegawai');
            $tgl_pinjam = $this->input ->post('tgl_pinjam');
            $tgl_kembali = $this->input ->post('tgl_kembali');
            $status = $this->input->post('status_peminjaman');
        
            $data = array(
                'id_pegawai' => $id_pegawai,
                'tgl_pinjam' => $tgl_pinjam,
                'tgl_kembali' => $tgl_kembali,
                'status_peminjaman' => $status
                );
           // var_dump($data);die;
            $id_peminjaman = $this->Op_model->input('peminjaman', $data);

            $id_inventaris = $this->input->post('id_inventaris');
            $jumlah = $this->input->post('jumlah');

            $index=0;

            $data = array(
                'id_peminjaman' => $id_peminjaman,
                'id_inventaris' => $id_inventaris,
                'jumlah' => $jumlah
              );

            $this->db->insert('detail_pinjam',$data);
            redirect('Operator/peminjaman');
        }


        function d_peminjaman($id){
        
            $data['judul'] = 'O.Detail Peminjaman';
            $where = array('id_peminjaman'=>$id);
            $data['vpeminjaman'] = $this->Crud_model->detail_data($where, 'vpeminjaman');
            
            $this->load->view('templates/header2', $data);
            $this->load->view('operator/detail-peminjaman', $data);
        }

        function hapus_peminjaman($id){
            $where= array('id_peminjaman'=> $id);
            $this->Crud_model->hapus_data($where, 'peminjaman');
            redirect('Operator/peminjaman');
        }

        function u_peminjaman($id){
            $data['judul'] = 'O.U-peminjaman';
            $where = array('id_peminjaman'=>$id);
            $data['peminjaman'] = $this->Crud_model->edit_data($where, 'peminjaman')->result();
            $data['detail_pinjam'] = $this->Crud_model->edit_data($where, 'detail_pinjam')->result();

            
            $this->load->view('templates/header2', $data);
            $this->load->view('operator/u-peminjaman', $data);
        }

        function update_peminjaman(){
            //left=var bebas right=db
            $id = $this ->input->post('id_peminjaman');
            $id_pegawai = $this->input->post('id_pegawai');
            $tgl_pinjam = $this->input->post('tgl_pinjam');
            $tgl_kembali = $this->input->post('tgl_kembali');
            $status_peminjaman = $this->input->post('status_peminjaman');

            $data = array(
                'id_peminjaman'=>$id,
                'id_pegawai' => $id_pegawai,
                'tgl_pinjam' => $tgl_pinjam,
                'tgl_kembali' => $tgl_kembali,
                'status_peminjaman' => $status_peminjaman
                );


                $where = array('id_peminjaman'=>$id);
                $this->Crud_model->update_data($where,$data, 'peminjaman');
                redirect('operator/peminjaman');
        }

        function u_detail_pinjam($id){
            $data['judul'] = 'O.U-peminjaman';
            $where = array('id_detail_pinjam'=>$id);
            $data['detail_pinjam'] = $this->Crud_model->edit_data($where, 'detail_pinjam')->result();

            
            $this->load->view('templates/header2', $data);
            $this->load->view('operator/u-detail-pinjam', $data);
        }

        // function update_detail_pinjam(){
        //     //left=var bebas right=db
        //     $id = $this ->input->post('id_detail_pinjam');
        //     $id_peminjaman = $this->input->post('id_peminjaman');
        //     $id_inventaris = $this->input->post('id_inventaris');
        //     $jumlah = $this->input->post('jumlah');

        //     $data = array(
        //         'id_detail_pinjam'=>$id,
        //         'id_peminjaman' => $id_peminjaman,
        //         'id_inventaris' => $id_inventaris,
        //         'jumlah' => $jumlah
        //         );


        //         $where = array('id_detail_pinjam'=>$id);
        //         $this->Crud_model->update_data($where,$data, 'detail_pinjam');
        //         redirect('operator/peminjaman');
        // }





    // pengembalian=================================
        function pengembalian(){
        $data['judul'] = 'O.Pengembalian';
        $data['vpengembalian'] = $this->Op_model->getAllvpengembalian();

        $this->load->view('templates/header2', $data);
        $this->load->view('operator/pengembalian', $data);
        }

        function d_pengembalian($id){
            $data['judul'] = 'O.Detail pengembalian';
            $where = array('id_pengembalian'=>$id);
            $data['vpengembalian'] = $this->Crud_model->detail_data($where, 'vpengembalian');
            
            $this->load->view('templates/header2', $data);
            $this->load->view('operator/detail-pengembalian', $data);
        }

        function hapus_pengembalian($id){
            $where= array('id_pengembalian'=> $id);
            $this->Crud_model->hapus_data($where, 'pengembalian');
            redirect('Operator/pengembalian');
        }

        function cpengembalian(){
            $data['judul'] = 'O.C-Pegawai';
            $data['pegawai'] = $this->db->get('pegawai')->result_array();
            $data['peminjaman'] = $this->db->get('peminjaman')->result_array();


            $this->load->view('templates/header2', $data);
            $this->load->view('operator/c-pengembalian', $data);   
        }   

        function tambah_pengembalian(){
            $id = $this->input->post('id_pengembalian');
            $id_peminjaman = $this->input->post('id_peminjaman');
            $id_pegawai = $this->input->post('id_pegawai');
            $jumlah_kembali = $this->input->post('jumlah_kembali');     
            $tgl_kembali= $this->input->post('tgl_kembali');       
        
            $data = array(
                'id_peminjaman' => $id_peminjaman,
                'id_pegawai' => $id_pegawai,
                'jumlah_kembali' => $jumlah_kembali,
                'tgl_kembali' => $tgl_kembali
                );
                
            $this->db->insert('pengembalian', $data);
            redirect('Operator/pengembalian');
        }

        function u_pengembalian($id){
            $data['judul'] = 'O.U-pengembalian';
            $where = array('id_pengembalian'=>$id);
            $data['pengembalian'] = $this->Crud_model->edit_data($where, 'pengembalian')->result();
            
            $this->load->view('templates/header2', $data);
            $this->load->view('operator/u-pengembalian', $data);
        }

        function update_pengembalian(){
            //left=var bebas right=db
            $id = $this ->input->post('id_pengembalian');
            $id_pegawai = $this->input->post('id_pegawai');
            $id_peminjaman = $this->input->post('id_peminjaman');
            $jumlah_kembali = $this->input->post('jumlah_kembali');
            $tgl_kembali = $this->input->post('tgl_kembali');

            $data = array(
                'id_pengembalian'=>$id,
                'id_pegawai' => $id_pegawai,
                'id_peminjaman' => $id_peminjaman,
                'jumlah_kembali' => $jumlah_kembali,
                'tgl_kembali' => $tgl_kembali
                );


                $where = array('id_pengembalian'=>$id);
                $this->Crud_model->update_data($where,$data, 'pengembalian');
                redirect('operator/pengembalian');
        }


// setting======================================

    function setting(){
        $data['judul'] = 'Hal Settings A';
        $data['petugas'] = $this->Op_model->getAllruang();

        $this->load->view('templates/header2', $data);
        $this->load->view('operator/settings');
    }
}
?>