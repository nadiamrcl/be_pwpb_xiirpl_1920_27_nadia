<?php

class Admin extends CI_Controller {
    function __construct(){

    parent::__construct();

    $this->load->helper('url'); 
    $this->load->helper('form');
    $this->load->library('session');
    $this->load->library('form_validation');
    $this->load->model('Admin_model');
    $this->load->model('Crud_model');

// if($this->session->set_userdata('id_level') !="A1") {
//     redirect('landing/index');
// }  
    }

    function index(){
        // $data['user'] = $this->db->get_where('petugas', ['username' =>$this->session->userdata('username')])->row_array();
        
        $data['judul'] = 'Halaman Admin';
        $data['inventaris'] = $this->Admin_model->getAllbrg();
        $data['jml_pgw'] = $this->Admin_model->getAlljmlpgw(); 
        $data['jml_brg'] = $this->Admin_model->getAlljmlbrg();  
        $data['jum_pinjam'] = $this->Admin_model->getAlljmlpinjam(); 
        $data['jum_kembali'] = $this->Admin_model->getAlljmlkembali(); 

        $this->load->view('administrator/dashboard', $data);
        // $this->load->view('templates/footer');
    }
// brg=========================================
    function brg(){
        $data['judul'] = 'Halaman Barang';
        $data['inventaris'] = $this->Admin_model->getAllbrg();
        $data['vinven'] = $this->Admin_model->getAllviewbrg();


        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/brg',$data);
        // $this->load->view('templates/footer');
    }

    function cbrg(){
        $data['judul'] = 'Hal C-Barang';
        $this->form_validation->set_rules('nama', 'Nama', 'required');        
        
        $sel['ruang'] = $this->db->get('ruang')->result_array();
        $sel['jenis'] = $this->db->get('jenis')->result_array();
        $sel['petugas'] = $this->db->get('petugas')->result_array();
        if(
            $this->form_validation->run() == FALSE ) {
        
        $this->load->view('templates/header2', $data, $sel);
        $this->load->view('administrator/c-brg', $sel, $data);
        // $this->load->view('templates/footer', $sel);
        }else{
           $this->Admin_model->cbrg();
           redirect('brg');
                }
        }
            
    function tambah_aksi(){
      $config['upload_path']   = 'assets/img/barang/';
      $config['allowed_types']   = 'gif|jpg|png|jpeg';
      $config['max_size']         = 10000;
      
      
      $this->load->library('upload', $config);
        $post   = $_FILES['gambar'];
        $file_data =array(
            "name" =>$post["name"],
            "type" =>$post["type"],
            "tmp_name" =>$post["tmp_name"],
            "error" =>$post["error"],
            "size" =>$post["size"]
        );
        // var_dump( $post);die();

        $dataUpload  =$file_data;
        $nama_file  ="brg"."-".time()."-".str_replace("", "_", $post['name']);

       // var_dump($nama_file);die();
        move_uploaded_file($post['tmp_name'], 'assets/img/barang/'.$nama_file);

        $id_ruang = $this->input->post('id_ruang');
		$id_ptg = $this->input->post('id_ptg');
        $id_jenis = $this->input->post('id_jenis');
		$nama = $this->input->post('nama');
        $kondisi = $this->input->post('kondisi');
        $ket = $this->input->post('ket');
        $jumlah = $this->input->post('jumlah');
        $kode = $this->input->post('kode');
        $tgl_regis = $this->input->post('tgl_regis');
        $gambar = $this->input->post('gambar');


		$data = array(
            'id_ruang' => $id_ruang,
            'id_ptg' => $id_ptg,
            'id_jenis' => $id_jenis,
            'nama' => $nama,
            'kondisi' => $kondisi,
            'keterangan' => $ket,
            'jumlah' => $jumlah,
            'kode_inventaris' => $kode,
			'tgl_regis' => $tgl_regis,
			'gambar' => $nama_file 
            );
            
		$this->Crud_model->input_data($data,'inventaris');
		redirect('Admin/brg');
	}


    function hapus_brg($id){
        $where= array('id_inventaris'=> $id);
        $this->Crud_model->hapus_data($where, 'inventaris');
        redirect('Admin/brg');
    }

    function ubrg($id){
        $data['ruang'] = $this->db->get('ruang')->result_array();
        $data['jenis'] = $this->db->get('jenis')->result_array();
        $data['petugas'] = $this->db->get('petugas')->result_array();
        
        $data['judul'] = 'Hal U-Barang';
        $where = array('id_inventaris'=>$id);
        $data['inventaris'] = $this->Crud_model->edit_data($where, 'inventaris')->result();
        
        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/u-brg', $data);
        // $this->load->view('templates/footer');

    }

    function update_brg(){
        $config['upload_path']   = 'assets/img/barang/';
          $config['allowed_types']   = 'gif|jpg|png|jpeg';
          $config['max_size']         = 10000;
          
          
          $this->load->library('upload', $config);
            $post   = $_FILES['gambar'];
            $file_data =array(
                "name" =>$post["name"],
                "type" =>$post["type"],
                "tmp_name" =>$post["tmp_name"],
                "error" =>$post["error"],
                "size" =>$post["size"]
            );
            // var_dump( $post);die();

            $dataUpload  =$file_data;
            $nama_file  ="brg"."-".time()."-".str_replace("", "_", $post['name']);

           // var_dump($nama_file);die();
            move_uploaded_file($post['tmp_name'], 'assets/img/barang/'.$nama_file);
            // $row = $this->model_app->view_where('users',array('username' => $this->input->post('id')))->row();

            // unlink('assets/img/barang/'.$row->gambar);

        $id = $this ->input->post('id_inventaris');
        $id_ruang = $this->input->post('id_ruang');
		$id_ptg = $this->input->post('id_ptg');
        $id_jenis = $this->input->post('id_jenis');
		$nama = $this->input->post('nama');
        $kondisi = $this->input->post('kondisi');
        $ket = $this->input->post('ket');
        $jumlah = $this->input->post('jumlah');
        $kode = $this->input->post('kode');
        $tgl_regis = $this->input->post('tgl_regis');
        $gambar = $this->input->post('gambar');


		$data = array(
            'id_ruang' => $id_ruang,
            'id_ptg' => $id_ptg,
            'id_jenis' => $id_jenis,
            'nama' => $nama,
            'kondisi' => $kondisi,
            'keterangan' => $ket,
            'jumlah' => $jumlah,
            'kode_inventaris' => $kode,
			'tgl_regis' => $tgl_regis,
			'gambar' => $nama_file
            );
            $where = array('id_inventaris'=>$id);
            $this->Crud_model->update_data($where,$data, 'inventaris');
            redirect('Admin/brg');
    }

    function dbrg($id){
        
        
        $data['judul'] = 'A. Detail Barang';
        $where = array('id_inventaris'=>$id);
        $data['vinventaris'] = $this->Crud_model->detail_data($where, 'vinventaris');
            
        
        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/detail-brg', $data);
    

    }

   public function print(){
       $data['inventaris'] = $this->Crud_model->tampil_data('inventaris')->result();
       $this->load->view('administrator/print_brg', $data);

    }

// pgw==========================================
    function pgw(){
        $data['judul'] = 'Hal Pegawai A';
        $data['pgw'] = $this->Admin_model->getAllpgw();

        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/pgw', $data);
        // $this->load->view('templates/footer');
    }
    
    function hapus_pgw($id){
        $where= array('id_pegawai'=> $id);
        $this->Crud_model->hapus_data($where, 'pegawai');
        redirect('Admin/pgw');
    }

    function dpgw($id){
        
        $data['judul'] = 'A. Detail pegawai';
        $where = array('id_pegawai'=>$id);
        $data['pegawai'] = $this->Crud_model->detail_data($where, 'pegawai');
        
        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/detail-pgw', $data);
        // $this->load->view('templates/footer');

    }

    function cpgw(){
        $data['judul'] = 'Hal C-Pegawai';
       
        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/c-pgw', $data);
        // $this->load->view('templates/footer');       
    }   

    function tambah_pgw(){
      $config['upload_path']   = 'assets/img/pegawai/';
      $config['allowed_types']   = 'gif|jpg|png|jpeg';
      $config['max_size']         = 10000;
      
      
      $this->load->library('upload', $config);
        $post   = $_FILES['gambar'];
        $file_data =array(
            "name" =>$post["name"],
            "type" =>$post["type"],
            "tmp_name" =>$post["tmp_name"],
            "error" =>$post["error"],
            "size" =>$post["size"]
        );
        // var_dump( $post);die();

        $dataUpload  =$file_data;
        $nama_file  ="barang"."-".time()."-".str_replace("", "_", $post['name']);

       // var_dump($nama_file);die();
        move_uploaded_file($post['tmp_name'], 'assets/img/pegawai/'.$nama_file);

        $id = $this->input->post('id_pegawai');
        $nama = $this->input->post('nama_pgw');
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $pass = $this->input->post('password');
        $nip = $this->input->post('nip');
        $gambar = $this->input->post('gambar');
        $alamat = $this->input->post('alamat');
    
		$data = array(
            'nama_pgw' => $nama,
            'username' => $username,
            'email' => $email,
            'password' => password_hash($pass, PASSWORD_DEFAULT),
            'nip' => $nip,
            'gambar' => $nama_file,
            'alamat' => $alamat
            );
            
        $this->db->insert('pegawai', $data);
        redirect('Admin/pgw');
    }

    function upgw($id){
        $data['judul'] = 'Hal U-Pegawai';
        $where = array('id_pegawai'=>$id);
        $data['pegawai'] = $this->Crud_model->edit_data($where, 'pegawai')->result();

        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/u-pgw', $data);
        // $this->load->view('templates/footer');           
    }

    function update_pgw(){
        $id = $this->input->post('id_pegawai');
        $nama = $this->input->post('nama_pgw');
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $pass = $this->input->post('password');
        $nip = $this->input->post('nip');
        $gambar = $this->input->post('gambar');
        $alamat = $this->input->post('alamat');
    
		$data = array(
            'nama_pgw' => $nama,
            'username' => $username,
            'email' => $email,
            'password' => $pass,
            'nip' => $nip,
            'gambar' => $gambar,
            'alamat' => $alamat
            );

            $where = array('id_pegawai'=>$id);
            $this->Crud_model->update_data($where,$data, 'pegawai');
            redirect('Admin/pgw');
    }
    
    

// peminjaman===================================

    function peminjaman(){
        $data['judul'] = 'Hal Peminjaman A';
        $data['peminjaman'] = $this->Admin_model->getAllpeminjaman();
        $data['vpeminjaman'] = $this->Admin_model->getAllvpeminjaman();



        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/peminjaman', $data);
        // $this->load->view('templates/footer');
    }

    function d_peminjaman($id){
        
        $data['judul'] = 'A. Detail Peminjaman';
        $where = array('id_peminjaman'=>$id);          
        $data['vpeminjaman'] = $this->Crud_model->detail_data($where, 'vpeminjaman');

        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/detail-peminjaman', $data);
        // $this->load->view('templates/footer');
    }
    public function print_pinjam(){
        $data['peminjaman'] = $this->Crud_model->print('peminjaman')->result();
        $this->load->view('administrator/print_pinjam', $data);
 
     }
// pengembalian=================================

    function pengembalian(){
        $data['judul'] = 'Hal Pengembalian A';
        // $data['pengembalian'] = $this->->getAllpengembalian();
        $data['vpengembalian'] = $this->Admin_model->getAllvpengembalian();

        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/pengembalian', $data);
        // $this->load->view('templates/footer');
    }

    function d_pengembalian($id){
        
        
        $data['judul'] = 'A. Detail pengembalian';
        $where = array('id_pengembalian'=>$id);
        $data['vpengembalian'] = $this->Crud_model->detail_data($where, 'vpengembalian');
        
        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/detail-pengembalian', $data);
        $this->load->view('templates/footer');

    }
// op===========================================

    function op(){
        $data['judul'] = 'Halaman Op A';
        $data['petugas'] = $this->Admin_model->getAllptg();

        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/oprator',$data);
        // $this->load->view('templates/footer');
    }

    function cop(){
        $data['level'] = $this->db->get('level')->result_array();
        $data['judul'] = 'A Oprator';
        // $where = array('id_level'=>$id_level == 2);

        $this->load->view('templates/header2',$data);
        $this->load->view('administrator/c-oprator', $data);

    }

    function tambah_op(){
        $id = $this ->input->post('id_petugas');
        $id_level = $this->input->post('id_level');
        $username = $this->input->post('username');
        $pass = $this->input->post('password');
        $email = $this->input->post('email');
        $gam = $this->input->post('gambar');
        $nama = $this->input->post('nama_ptg');

		$data = array(
            'id_level' => $id_level,
            'username' => $username,
            'password' => password_hash($pass, PASSWORD_DEFAULT),
            'email' => $email,
            'gambar' => $gam,
            'nama_ptg' => $nama
            
            );
        $this->db->insert('petugas', $data);
		redirect('Admin/op');
	}

    function uop($id){
        $data['level'] = $this->db->get('level')->result_array();
        $data['judul'] = 'A Oprator';
        $where = array('id_ptg'=>$id);
        $data['petugas'] = $this->Crud_model->edit_data($where, 'petugas')->result();
        
        $this->load->view('templates/header2',$data);
        $this->load->view('administrator/u-oprator', $data);
        // $this->load->view('templates/footer');
    }

    function update_op(){
        //left=var bebas right=db
        $id = $this ->input->post('id_ptg');
        $id_level = $this->input->post('id_level');
        $username = $this->input->post('username');
        $pass = $this->input->post('password');
        $email = $this->input->post('email');
        $gam = $this->input->post('gambar');
        $nama = $this->input->post('nama_ptg');

		$data = array(
            'id_ptg'=>$id,
            'id_level' => $id_level,
            'username' => $username,
            'password' => $pass,
            'email' => $email,
            'gambar' => $gam,
            'nama_ptg' => $nama
            );

            $where = array('id_ptg'=>$id);
            $this->Crud_model->update_data($where,$data, 'petugas');
            redirect('Admin/op');
    }

    function hapus_op($id){
        $where= array('id_ptg'=> $id);
        $this->Crud_model->hapus_data($where, 'petugas');
        redirect('Admin/op');
    }

    function d_op($id){
        
        $data['judul'] = 'A. Detail Operator';
        $where = array('id_ptg'=>$id);
        $data['petugas'] = $this->Crud_model->detail_data($where, 'petugas');
        
        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/detail-op', $data);
    

    }
// ruang========================================

    function ruang(){
        $data['judul'] = 'Hal Ruang A';
        $data['ruang'] = $this->Admin_model->getAllruang();

        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/ruang', $data);
        // $this->load->view('templates/footer');
    }

    function cruang(){
        $data['judul'] = 'Hal Ruang A';

        $this->load->view('templates/header2');
        $this->load->view('administrator/c-ruang',$data);
        // $this->load->view('templates/footer');
    }

    function tambah_ruang(){
        $id = $this ->input->post('id_ruang');
		$nama = $this->input->post('nama_ruang');
        $kode = $this->input->post('kode_ruang');
        $ket = $this->input->post('keterangan');
		$data = array(
            'nama_ruang' => $nama,
            'kode_ruang' => $kode,
            'keterangan' => $ket
            );
        $this->db->insert('ruang', $data);
		redirect('Admin/ruang');
	}

    function hapus_ruang($id){
        $where= array('id_ruang'=> $id);
        $this->Crud_model->hapus_data($where, 'ruang');
        redirect('Admin/ruang');
    }

    function u_ruang($id){
        $data['judul'] = 'A U-Ruang';
        $where = array('id_ruang'=>$id);
        $data['ruang'] = $this->Crud_model->edit_data($where, 'ruang')->result();
        
        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/u-ruang', $data);
        // $this->load->view('templates/footer');

    }

    function update_ruang(){
        //left=var bebas right=db
        $id = $this ->input->post('id_ruang');
		$nama = $this->input->post('nama_ruang');
        $kode = $this->input->post('kode_ruang');
        $ket = $this->input->post('keterangan');
		$data = array(
            'id_ruang'=>$id,
            'nama_ruang' => $nama,
            'kode_ruang' => $kode,
            'keterangan' => $ket
            );

            $where = array('id_ruang'=>$id);
            $this->Crud_model->update_data($where,$data, 'ruang');
            redirect('Admin/ruang');
    }

    function d_ruang($id){
        
        $data['judul'] = 'A. Detail ruang';
        $where = array('id_ruang'=>$id);
        $data['ruang'] = $this->Crud_model->detail_data($where, 'ruang');
        
        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/detail-ruang', $data);
    }
// setting======================================

    function setting(){
        $data['judul'] = 'Hal Settings A';
        $data['petugas'] = $this->Admin_model->getAllptg();

        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/settings');
        // $this->load->view('templates/footer');
    }
// j-brg========================================

    function j_brg(){
        $data['judul'] = 'Hal Ruang A';
        $data['jenisbrg'] = $this->Admin_model->getAlljenisbrg();

        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/jenisbrg', $data);
        // $this->load->view('templates/footer');
    }

    function cj_brg(){
        $data['judul'] = 'jenisBarang A';

        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/c-jenisbrg', $data);
        // $this->load->view('templates/footer');  

    }

    function tambah_jbrg(){
        $id = $this ->input->post('id_jenis');
		$nama = $this->input->post('nama');
        $kode = $this->input->post('kode');
        $ket = $this->input->post('ket');
		$data = array(
            'nama_jenis' => $nama,
            'kode_jenis' => $kode,
            'keterangan' => $ket
            );
        $this->db->insert('jenis', $data);
		redirect('Admin/J_brg');
	}
   
    function hapus_jbrg($id){
        $where= array('id_jenis'=> $id);
        $this->Crud_model->hapus_data($where, 'jenis');
        redirect('Admin/j_brg');
    }
    
    function d_jbrg($id){
        
        $data['judul'] = 'A. Detail Jenis';
        $where = array('id_jenis'=>$id);
        $data['jenis'] = $this->Crud_model->detail_data($where, 'jenis');
        
        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/detail_jenisbrg', $data);
        // $this->load->view('templates/footer');

    }


    function uj_brg($id){
        $data['judul'] = 'A U-Jenisbrg';
        $where = array('id_jenis'=>$id);
        $data['jenis'] = $this->Crud_model->edit_data($where, 'jenis')->result();
        
        $this->load->view('templates/header2', $data);
        $this->load->view('administrator/u-jenisbrg', $data);
        // $this->load->view('templates/footer');

    }

    function update_jbrg(){
        //left=var bebas right=db
        $id = $this ->input->post('id_jenis');
		$nama = $this->input->post('nama_jenis');
        $kode = $this->input->post('kode_jenis');
        $ket = $this->input->post('keterangan');
		$data = array(
            'id_jenis'=>$id,
            'nama_jenis' => $nama,
            'kode_jenis' => $kode,
            'keterangan' => $ket
            );

            $where = array('id_jenis'=>$id);
            $this->Crud_model->update_data($where,$data, 'jenis');
            redirect('Admin/j_brg');
    }

}
?>


