<?php

class Pegawai extends CI_Controller {
    function __construct(){

    parent::__construct();

    $this->load->library('form_validation');
    $this->load->helper('url');
    $this->load->helper('form');
    $this->load->model('Pegawai_model');
    $this->load->model('Crud_model');
    $this->load->library('session');
    
    }

    function index(){
        $data['vinven_nol'] = $this->Pegawai_model->getAllvbrg();

        $this->load->view('pegawai/brg', $data);
    }

    function dbrg($id){
        $data['judul'] = 'P.Detail Barang';
        $where = array('id_inventaris'=>$id);
        $data['vinventaris'] = $this->Crud_model->detail_data($where, 'vinventaris');
            
        $this->load->view('templates/header2', $data);
        $this->load->view('Pegawai/detail-brg', $data);
    }    


    //peminjaman

    function peminjaman($id){
        $data['judul'] = 'P.C-peminjaman';
        $where = array('id_inventaris'=>$id);
        $data['inventaris'] = $this->Crud_model->detail_data($where, 'inventaris');

        $this->load->view('pegawai/c-peminjaman', $data);   
    }


    function tambah_peminjaman(){
            // $id_pegawai = $this->input->post('id_pegawai');
            // $tgl_pinjam = $this->input ->post('tgl_pinjam');
            $tgl_kembali = $this->input ->post('tgl_kembali');
            // $status = $this->input->post('status_peminjaman');
        
            $data = array(
                'id_pegawai' => $this->session->userdata('id_pegawai'),
                'tgl_pinjam' =>  date('Y-m-d'),
                'status_peminjaman' => 'Dipinjamkan',            
                'tgl_kembali' => $tgl_kembali,

                );
           // var_dump($data);die;
            $id_peminjaman = $this->Op_model->input( 'peminjaman',$data);

            $id_inventaris = $this->input->post('id_inventaris');
            $jumlah = $this->input->post('jumlah');

            $index=0;

            $data = array(
                'id_peminjaman' => $id_peminjaman,
                'id_inventaris' => $id_inventaris,
                'jumlah' => $jumlah
              );

            $this->db->insert('detail_pinjam',$data);
            redirect('Pegawai/index');
        }

// setting======================================

    function settings(){
        $data['judul'] = 'Hal Settings P';
        $data['petugas'] = $this->Pegawai_model->getAllptg();

        $this->load->view('pegawai/settings');
    }
}
?>


