<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('My_model');
        $this->load->library('session');

    }

    public function index()
    {   
        $this->load->view('templates/header');
        $this->load->view('landing/index');
        $this->load->view('templates/footer');

    }


    function proses_login(){


        $user=$this->input->post('username');
        $pass=$this->input->post('password');
        $where=array(
            'username'=> $user,
            'password'=> $pass,
        );


        $userpegawai= $this->db->get_where('pegawai', ['username'=>$user])->row_array();
        if($userpegawai){
            //cek pass
            if(password_verify($pass,$userpegawai['password']) ){
                $data= [
                    'username'=> $userpegawai ['username'],
                    'id_level'=> $userpegawai ['id_level'],
                    'gambar' => $userpegawai ['gambar'],
                    'id_pegawai' => $userpegawai ['id_pegawai'],
                    'email' =>$userpegawai ['email'],
                    'nama_pgw' =>$userpegawai ['nama_pgw'],
                    'nip' =>$userpegawai ['nip'],
                ];
                $this->session->set_userdata($data);
                redirect('pegawai/index');
            }
        }else{
            $userpetugas= $this->db->get_where('petugas', ['username'=>$user])->row_array();
            if($userpetugas){
                //cek pass                if (password_verify($pass ,$userpetugas['password'])&& $userpetugas['id_level']== "2"){

            if (password_verify($pass ,$userpetugas['password'])&& $userpetugas['id_level']== "2"){                    $data= [
                        'username'=> $userpetugas ['username'],
                        'id_level'=> $userpetugas ['id_level'],
                        'gambar' => $userpetugas ['gambar'],
                        'id_ptg' => $userpetugas ['id_ptg'],
                        'email' =>$userpetugas ['email'],
                        'nama_ptg' =>$userpetugas ['nama_ptg'],                        
                    ];
                    $this->session->set_userdata($data);
                    redirect('Operator/index');
        } else{
            $userpetugas= $this->db->get_where('petugas', ['username'=>$user])->row_array();
            if($userpetugas){
                //cek pass
                if ($userpetugas['password']== $pass && $userpetugas['id_level']== "1"){
                    $data= [
                        'username'=> $userpetugas ['username'],
                        'id_level'=> $userpetugas ['id_level'],
                        'gambar' => $userpetugas ['gambar'],
                        'id_ptg' => $userpetugas ['id_ptg'],
                        'email' =>$userpetugas ['email'],
                        'nama_ptg' =>$userpetugas ['nama_ptg'],
                    ];  
                    $this->session->set_userdata($data);
                    redirect('Admin/index'); 
                    }else{
                        $data['pesan']="Username atau Password tidak sesuai.";
                        $this->load->view('templates/header',$data);
                        }
            }
        }
    }
}
}
}