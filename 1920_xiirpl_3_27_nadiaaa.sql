-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2019 at 06:31 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `1920_xiirpl_3_27_nadiaaa`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `Addpegawai` (IN `id_pegawai` VARCHAR(6), IN `nama_pgw` VARCHAR(50), IN `nip` INT, IN `alamat` TEXT)  begin
insert into pegawai values(id_pegawai,nama_pgw,nip,alamat); end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(6) NOT NULL,
  `id_inventaris` int(6) NOT NULL,
  `id_pegawai` int(6) NOT NULL,
  `jumlah` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail_pinjam`, `id_inventaris`, `id_pegawai`, `jumlah`) VALUES
(1, 1, 1, 10),
(2, 5, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `inventaris`
--

CREATE TABLE `inventaris` (
  `id_inventaris` int(6) NOT NULL,
  `id_ptg` int(6) NOT NULL,
  `id_jenis` int(6) NOT NULL,
  `id_ruang` int(6) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `kondisi` varchar(20) NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tgl_regis` date NOT NULL,
  `kode_inventaris` varchar(10) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `id_ptg`, `id_jenis`, `id_ruang`, `nama`, `kondisi`, `keterangan`, `jumlah`, `tgl_regis`, `kode_inventaris`, `gambar`) VALUES
(1, 1, 1, 2, 'Bom', 'Layak pakai', 'Layak pakai dll', 67, '2019-07-05', 'B01', ''),
(3, 1, 6, 5, 'Bom', 'Layak pakai', 'Layak pakai dll', 15, '2019-07-05', 'B01', 'face4.jpg'),
(5, 1, 2, 1, 'cccc', 'cccc', 'cccccc', 11, '2019-09-19', 'B08', '');

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(6) NOT NULL,
  `nama_jenis` varchar(50) DEFAULT NULL,
  `kode_jenis` varchar(3) DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(1, 'Alat Olahraga', 'A01', NULL),
(2, 'Alat Praga', 'A02', NULL),
(3, 'Alat Pecah belah', 'A03', NULL),
(5, 'Alat Perkakas', 'A05', NULL),
(6, 'Alat  Listrik Besar & kecil', '7', '');

-- --------------------------------------------------------

--
-- Stand-in structure for view `jlev_ptg`
-- (See below for the actual view)
--
CREATE TABLE `jlev_ptg` (
`id_ptg` int(6)
,`nama_level` enum('administrator','operator','','')
,`nama_ptg` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `jml_pgw`
-- (See below for the actual view)
--
CREATE TABLE `jml_pgw` (
`count(*)` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(6) NOT NULL,
  `nama_level` enum('administrator','operator','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'administrator'),
(2, 'operator');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(6) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(10) NOT NULL,
  `nama_pgw` varchar(100) DEFAULT NULL,
  `nip` int(11) DEFAULT NULL,
  `alamat` text,
  `email` varchar(50) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `username`, `password`, `nama_pgw`, `nip`, `alamat`, `email`, `gambar`) VALUES
(1, 'pegawai', '000000', 'Pegawai1', 101, 'jjjjj', 'vraka@gmail.com', 'face4.jpg'),
(4, 'Zulfasafilaaah', 'zulfa000', 'Zulfa Safilah', 566, 'jl. pangandaran 5', 'zulfas203@gmail.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(6) NOT NULL,
  `id_detail_pinjam` int(6) NOT NULL,
  `id_pegawai` int(6) NOT NULL,
  `tgl_pinjam` datetime NOT NULL,
  `tgl_kembali` datetime NOT NULL,
  `status_peminjaman` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `id_detail_pinjam`, `id_pegawai`, `tgl_pinjam`, `tgl_kembali`, `status_peminjaman`) VALUES
(1, 1, 1, '2018-11-04 00:00:00', '2019-07-18 13:00:00', 'dipinjamkan'),
(2, 2, 1, '2018-11-04 00:00:00', '2018-11-17 00:00:00', 'dipinjamkan');

-- --------------------------------------------------------

--
-- Table structure for table `pengembalian`
--

CREATE TABLE `pengembalian` (
  `id_pengembalian` int(6) NOT NULL,
  `id_peminjaman` int(6) NOT NULL,
  `id_pegawai` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengembalian`
--

INSERT INTO `pengembalian` (`id_pengembalian`, `id_peminjaman`, `id_pegawai`) VALUES
(1, 1, 1),
(2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_ptg` int(6) NOT NULL,
  `id_level` int(6) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `password` varchar(6) NOT NULL,
  `nama_ptg` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_ptg`, `id_level`, `username`, `email`, `gambar`, `password`, `nama_ptg`) VALUES
(1, 1, 'admin', '', '', '122345', 'Admsekolah'),
(2, 2, 'Oprator1', '', '', '123456', 'OPsekolah');

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(6) NOT NULL,
  `nama_ruang` varchar(255) DEFAULT NULL,
  `kode_ruang` varchar(3) DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(1, 'Ruang Alat Praga', 'R01', ''),
(2, 'Ruang Prasarana', 'R02', ''),
(3, 'Ruang Perkakas (gudang)', 'R03', NULL),
(5, 'Ruang Alat Olahraga', 'R05', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `subqany`
-- (See below for the actual view)
--
CREATE TABLE `subqany` (
`id_peminjaman` int(6)
,`id_pegawai` int(6)
,`tgl_pinjam` datetime
,`tgl_kembali` datetime
,`status_peminjaman` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vinven`
-- (See below for the actual view)
--
CREATE TABLE `vinven` (
`id_inventaris` int(6)
,`nama_jenis` varchar(50)
,`nama_ruang` varchar(255)
,`keterangan` text
,`jumlah` int(11)
);

-- --------------------------------------------------------

--
-- Structure for view `jlev_ptg`
--
DROP TABLE IF EXISTS `jlev_ptg`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `jlev_ptg`  AS  select `petugas`.`id_ptg` AS `id_ptg`,`level`.`nama_level` AS `nama_level`,`petugas`.`nama_ptg` AS `nama_ptg` from (`petugas` join `level`) where (`level`.`id_level` = `petugas`.`id_level`) ;

-- --------------------------------------------------------

--
-- Structure for view `jml_pgw`
--
DROP TABLE IF EXISTS `jml_pgw`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `jml_pgw`  AS  select count(0) AS `count(*)` from `pegawai` where (`pegawai`.`id_pegawai` <> 0) ;

-- --------------------------------------------------------

--
-- Structure for view `subqany`
--
DROP TABLE IF EXISTS `subqany`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `subqany`  AS  select `peminjaman`.`id_peminjaman` AS `id_peminjaman`,`peminjaman`.`id_pegawai` AS `id_pegawai`,`peminjaman`.`tgl_pinjam` AS `tgl_pinjam`,`peminjaman`.`tgl_kembali` AS `tgl_kembali`,`peminjaman`.`status_peminjaman` AS `status_peminjaman` from `peminjaman` where `peminjaman`.`tgl_kembali` > any (select `peminjaman`.`tgl_kembali` from `peminjaman` where (`peminjaman`.`tgl_kembali` < '2019-07-18 13:00:00')) ;

-- --------------------------------------------------------

--
-- Structure for view `vinven`
--
DROP TABLE IF EXISTS `vinven`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vinven`  AS  select `inventaris`.`id_inventaris` AS `id_inventaris`,`jenis`.`nama_jenis` AS `nama_jenis`,`ruang`.`nama_ruang` AS `nama_ruang`,`inventaris`.`keterangan` AS `keterangan`,`inventaris`.`jumlah` AS `jumlah` from ((`inventaris` join `ruang`) join `jenis`) where ((`jenis`.`id_jenis` = `inventaris`.`id_jenis`) and (`ruang`.`id_ruang` = `inventaris`.`id_ruang`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id_detail_pinjam`),
  ADD KEY `id_inventaris` (`id_inventaris`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`),
  ADD KEY `id_ptg` (`id_ptg`),
  ADD KEY `id_jenis` (`id_jenis`),
  ADD KEY `id_ruang` (`id_ruang`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`),
  ADD KEY `id_detail_pinjam` (`id_detail_pinjam`);

--
-- Indexes for table `pengembalian`
--
ALTER TABLE `pengembalian`
  ADD PRIMARY KEY (`id_pengembalian`),
  ADD KEY `id_peminjaman` (`id_peminjaman`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_ptg`),
  ADD KEY `id_level` (`id_level`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id_detail_pinjam` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id_inventaris` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pengembalian`
--
ALTER TABLE `pengembalian`
  MODIFY `id_pengembalian` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_ptg` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD CONSTRAINT `id_inventaris` FOREIGN KEY (`id_inventaris`) REFERENCES `inventaris` (`id_inventaris`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_pegawai` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD CONSTRAINT `id_jenis` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_ptg` FOREIGN KEY (`id_ptg`) REFERENCES `petugas` (`id_ptg`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_ruang` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD CONSTRAINT `id_detail_pinjam` FOREIGN KEY (`id_detail_pinjam`) REFERENCES `detail_pinjam` (`id_detail_pinjam`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pengembalian`
--
ALTER TABLE `pengembalian`
  ADD CONSTRAINT `id_peminjaman` FOREIGN KEY (`id_peminjaman`) REFERENCES `peminjaman` (`id_peminjaman`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `petugas`
--
ALTER TABLE `petugas`
  ADD CONSTRAINT `id_level` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
